#include "Canny.h"

using std::tuple;
using std::get;
using std::make_tuple;

Canny::Canny(const Image &src, uint th1, uint th2): source(src), result(src.n_rows, src.n_cols),
												h(src.n_rows), w(src.n_cols), t1(th1), t2(th2),
												G(h, w), visited(h, w){
	source = gaussian_separable(src, 1.4, 2);
	Sobel Ix = Ssobel_x(source), Iy = Ssobel_y(source);
	Matrix<double> Gd(h, w); // gradient direstions
	// cout << "beginning canny" << endl;
	for(uint x = 0; x < h; x++) // calculating gradients
		for(uint y = 0; y < w; y++){
			// if(x == y)
   //              std::cout << get<0>( Ix(x, y) ) << ':' << get<0>( Iy(x, y) )  << std::endl; 
			G(x, y) = sqrt(pow( get<0>( Ix(x, y) ), 2) + pow( get<0>( Iy(x, y) ), 2));
			Gd(x, y) = atan2(get<0>( Ix(x, y) ), get<0>( Iy(x, y) ));
			// cout << G(x, y) << ':' << get<0>( Ix(x, y) ) << ':' << get<0>( Iy(x, y) ) << endl;
		}
	// cout << "beginning 1" << endl;
	Matrix<double> Gsup = G.deep_copy();
	for(uint x = 0; x < h; x++) // suppressing non-max
		for(uint y = 0; y < w; y++){
			double Gdeg = Gd(x, y);
			double Grad = G(x, y), N0rad = 0, N1rad = 0;

			// if(x == y)
   //              std::cout << Gdeg << std::endl;

			int id = int((Gdeg - M_PI/8 + 2*M_PI) / (M_PI/4)) % 4;
					switch(id) {
						case 0: 
							N0rad = get_double(&G, x+1, y-1);
							N1rad = get_double(&G, x-1, y+1);
							break; 
						case 1:
							N0rad = get_double(&G, x, y+1);
							N1rad = get_double(&G, x, y-1);
							break; 
						case 2:
							N0rad = get_double(&G, x+1, y+1);
							N1rad = get_double(&G, x-1, y-1);
							break; 
						case 3: 
							N0rad = get_double(&G, x+1, y);
							N1rad = get_double(&G, x-1, y);
							break;
						default: ; 
					}
					if(!(Grad > N0rad && Grad > N1rad))
						Gsup(x, y) = 0;
		}
	G = Gsup.deep_copy();
	for(uint i = 0; i < h; i++) 
		for(uint j = 0; j < w; j++){
			visited(i, j) = false;
		}
	for(uint x = 0; x < h; x++)  // two thresholds
		for(uint y = 0; y < w; y++){
			if(G(x, y) <= t1){
				result(x, y) = make_tuple(0, 0, 0);
				// visited(x, y) = true;
			} else if(G(x, y) > t2){
				result(x, y) = make_tuple(-1, -1, -1); // white
				// visited(x, y) = true;
		}
	}
	for(uint x = 0; x < h; x++) // there goes edge detection itself
		for(uint y = 0; y < w; y++)
			if(!visited(x, y))
				is_edge(x, y);
}

bool Canny::is_edge(uint x, uint y){
	if(x >= h || y >= w) // our of borders
		return false;

	if(visited(x, y)){
		if(result(x, y) == make_tuple(0, 0, 0)){
			return false;
		} else if(result(x, y) == make_tuple(-1, -1, -1))
			return true;
	} else 
		visited(x, y) = true;

	if(result(x, y) == make_tuple(0, 0, 0)){
		return false;
	} else if(result(x, y) == make_tuple(-1, -1, -1)){
		return true;
	} else if(is_edge(x+1, y) || is_edge(x, y+1) || is_edge(x-1, y) || is_edge(x, y-1) ||
			  is_edge(x+1, y+1) || is_edge(x+1, y-1) || is_edge(x-1, y+1) || is_edge(x-1, y-1)){
		result(x, y) = make_tuple(-1, -1, -1);
		return true;
	} else {
		result(x, y) = make_tuple(0, 0, 0);
		return false;
	}

	return false;    
}