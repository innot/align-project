#include <iostream>
#include <algorithm>
#include <vector>
#include "functions.h"

using std::tuple;
using std::get;
using std::make_tuple;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::sort;

uint get_top_b_size(const Image &source){
	uint h = source.n_rows, w = source.n_cols;
	uint vert_q[h];
	for(uint i = 0; i < h; i++){ // calculate edge pixel quantities in each 
												 // possible top border row 
		vert_q[i] = 0;
		for(uint j = 0; j < w; j++)
			vert_q[i] += get<0>(get_pixel(&source, i, j)) ? 1 : 0;
	}
	uint m1 = 0, m2 = 0, m1i = 0, m2i = 0;
	for(uint i = 0; i < h; i++){ // find two maxs
		if(vert_q[i] >= m1){
			m2 = m1;
			m2i = m1i;
			m1 = vert_q[i];
			m1i = i;
		} else if(vert_q[i] >= m2){
			m2 = vert_q[i];
			m2i = i;
		}
	}
	if(m2i < m1i)
		m1i = m2i;
	const uint eps = 5;
	for(uint i = (m1i >= eps ? m1i - eps : 0); i < m1i + eps; i++)
		if(i != m1i && i < h)
			vert_q[i] = 0;

	m1 = 0, m2 = 0, m1i = 0, m2i = 0; // reinitiate search
	for(uint i = 0; i < h; i++){
		if(vert_q[i] >= m1){
			m2 = m1;
			m2i = m1i;
			m1 = vert_q[i];
			m1i = i;
		} else if(vert_q[i] >= m2){
			m2 = vert_q[i];
			m2i = i;
		}
	}
	return m1i > m2i ? m1i : m2i;
}

Borders get_borders(const Image &source){
	uint h = source.n_rows, w = source.n_cols;
	uint max_vert_brd_size = h / 20, max_hor_brd_size = w / 20;
	uint top_b = get_top_b_size(source.submatrix(0, 0, max_vert_brd_size, w));  // top border

	Image img_part(max_hor_brd_size, h); 										// right border 
	Image subm = source.submatrix(0, w - max_hor_brd_size, h, max_hor_brd_size);
	// cout << "img_part: " << img_part.n_rows << ':' <<  img_part.n_cols << endl;
	// cout << "subm: " << subm.n_rows << ':' << subm.n_cols << endl;
	// cout << "original: " << h << ':' << w << endl;
	for(uint i = 0; i < h; i++)
		for(uint j = 0; j < max_hor_brd_size; j++){
			// cout << "putting " << i << ':' << j << " to " << max_hor_brd_size-j-1 << ':' << i << endl;
			img_part(max_hor_brd_size - j - 1, i) = subm(i, j);
		}
	uint right_b = get_top_b_size(img_part);
	// cout << "right ok\n";

	img_part = Image(max_hor_brd_size, h); 										// left border 
	subm = source.submatrix(0, 0, h, max_hor_brd_size);
	for(uint i = 0; i < h; i++)
		for(uint j = 0; j < max_hor_brd_size; j++)
			img_part(j, i) = subm(i, j);
	uint left_b = get_top_b_size(img_part);
	// cout << "left ok\n";

	img_part = Image(max_vert_brd_size, w); 									// bottom border 
	subm = source.submatrix(h - max_vert_brd_size, 0, max_vert_brd_size, w);
	for(uint i = 0; i < max_vert_brd_size; i++)
		for(uint j = 0; j < w; j++)
			img_part(max_vert_brd_size - i - 1, j) = subm(i, j);
	uint bottom_b = get_top_b_size(img_part);
	Borders result = {top_b, right_b, bottom_b, left_b};
	// cout << "all ok\n";
	return result;
}

Pixel operator*(Pixel x, double y){
	return make_tuple(get<0>(x)*y, get<1>(x)*y, get<2>(x)*y);
}

Pixel operator+(Pixel x, Pixel y){
	return make_tuple(get<0>(x) + get<0>(y), get<1>(x) + get<1>(y), get<2>(x) + get<2>(y));
}

Pixel operator-(Pixel x, Pixel y){
	return make_tuple(max(0, min(get<0>(x) - get<0>(y), 255)), 
			max(0, min(get<1>(x) - get<1>(y), 255)), 
			max(0, min(get<2>(x) - get<2>(y), 255)));
}

Pixel normalize(Pixel x){
	return make_tuple(max(0, min(get<0>(x), 255)),
					  max(0, min(get<1>(x), 255)),
					  max(0, min(get<2>(x), 255)));
}

Pixel sqrt(Pixel x){
	return make_tuple(sqrt(get<0>(x)), sqrt(get<1>(x)), sqrt(get<2>(x)));
}

Image gray_world(Image &source){
	uint h = source.n_rows, w = source.n_cols;
	Image result(h, w);
	double Sr = 0, Sg = 0, Sb = 0;
	for(uint i = 0; i < h; i++){
		for(uint j = 0; j < w; j++){
			Sr += get<0>(source(i, j));
			Sg += get<1>(source(i, j));
			Sb += get<2>(source(i, j));
		}
		
	}
	// Sr /= h*w;
	// Sg /= h*w;
	// Sb /= h*w;
	// cout << "Srgb calced: " << Sr << Sg << Sb << endl;
	double S = (Sr + Sg + Sb)/3;
	// cout << "Srgb calced: " << Sr << '\t' << Sg << '\t'  << Sb << endl;
	for(uint i = 0; i < h; i++)
		for(uint j = 0; j < w; j++){
			double R, G, B;
			if(!int(round(Sr)))
				R = S/h/w;
			else
				R = max(0, min(round(get<0>(source(i, j))*S/Sr), 255));
			if(!int(round(Sg)))
				G = S/h/w;
			else
				G = max(0, min(round(get<1>(source(i, j))*S/Sg), 255));
			if(!int(round(Sb)))
				B = S/h/w;
			else
				B = max(0, min(round(get<2>(source(i, j))*S/Sb), 255));
			result(i, j) = make_tuple(R, G, B);
		}
	return result;
}

Image sobel_x(const Image &source, bool norm){
	Matrix<double> kernel = {{-1, 0, 1},
							 {-2, 0, 2},
							 {-1, 0, 1}};
	return custom(source, kernel, norm);
}

Image sobel_y(const Image &source, bool norm){
	Matrix<double> kernel = {{ 1,  2,  1},
							 { 0,  0,  0},
							 {-1, -2, -1}};
	return custom(source, kernel, norm);
}

Sobel Ssobel_x(const Image &source){
	Matrix<double> kernel = {{-1, 0, 1},
							 {-2, 0, 2},
							 {-1, 0, 1}};
	return customS(source, kernel);
}

Sobel Ssobel_y(const Image &source){
	Matrix<double> kernel = {{ 1,  2,  1},
							 { 0,  0,  0},
							 {-1, -2, -1}};
	return customS(source, kernel);
}

Pixel get_pixel(const Image *source, int row, int col){
	int n_rows = (*source).n_rows, n_cols = (*source).n_cols;
	if(row < 0){
		if(col < 0)
			return (*source)(abs(row) - 1, abs(col) - 1);
		else if (col >= n_cols)
			return (*source)(abs(row) - 1, n_cols - (col - n_cols) - 1);
		else 
			return (*source)(abs(row) - 1, col);
	} else if (row >= n_rows) {
		if(col < 0)
			return (*source)(n_rows - (row - n_rows) - 1, abs(col) - 1);
		else if (col >= n_cols)
			return (*source)(n_rows - (row - n_rows) - 1, n_cols - (col - n_cols) - 1);
		else 
			return (*source)(n_rows - (row - n_rows) - 1, col);
	} else {
		if(col < 0)
			return (*source)(row, abs(col) - 1);
		else if (col >= n_cols)
			return (*source)(row, n_cols - (col - n_cols) - 1);
		else 
			return (*source)(row, col);
	}
}

double get_double(const Matrix<double> *source, int row, int col){ // make template, wtf
	int n_rows = (*source).n_rows, n_cols = (*source).n_cols;
	if(row < 0){
		if(col < 0)
			return (*source)(abs(row) - 1, abs(col) - 1);
		else if (col >= n_cols)
			return (*source)(abs(row) - 1, n_cols - (col - n_cols) - 1);
		else 
			return (*source)(abs(row) - 1, col);
	} else if (row >= n_rows) {
		if(col < 0)
			return (*source)(n_rows - (row - n_rows) - 1, abs(col) - 1);
		else if (col >= n_cols)
			return (*source)(n_rows - (row - n_rows) - 1, n_cols - (col - n_cols) - 1);
		else 
			return (*source)(n_rows - (row - n_rows) - 1, col);
	} else {
		if(col < 0)
			return (*source)(row, abs(col) - 1);
		else if (col >= n_cols)
			return (*source)(row, n_cols - (col - n_cols) - 1);
		else 
			return (*source)(row, col);
	}
}

Image custom(const Image &source, Matrix<double> kernel, bool norm){
	uint h = source.n_rows, w = source.n_cols, kh = kernel.n_rows, kw = kernel.n_cols;
	Image result(h, w);
	if(!(kw%2) || !(kh%2))
		throw std::string("Incorrect kernel");
	uint kh2 = kh/2, kw2 = kw/2;
	for(uint x = 0; x < h; x++)
		for(uint y = 0; y < w; y++){
			double r = 0, g = 0, b = 0;
			for(uint i = 0; i < kh; i++)
				for(uint j = 0; j < kw; j++){
					double Tr = 0, Tg = 0, Tb = 0;
					std::tie(Tr, Tg, Tb) = get_pixel(&source, x+i-kh2, y+j-kw2);
					r += Tr*kernel(i, j);
					g += Tg*kernel(i, j);
					b += Tb*kernel(i, j);
				}
			result(x, y) = make_tuple(r, g, b);
			if(norm)
				result(x, y) = normalize(result(x, y));
		}        
	return result;  
}

Sobel customS(const Image &source, Matrix<double> kernel){
	uint h = source.n_rows, w = source.n_cols, kh = kernel.n_rows, kw = kernel.n_cols;
	Sobel result(h, w);
	if(!(kw%2) || !(kh%2))
		throw std::string("Incorrect kernel");
	uint kh2 = kh/2, kw2 = kw/2;
	for(uint x = 0; x < h; x++)
		for(uint y = 0; y < w; y++){
			double r = 0, g = 0, b = 0;
			for(uint i = 0; i < kh; i++)
				for(uint j = 0; j < kw; j++){
					double Tr = 0, Tg = 0, Tb = 0;
					std::tie(Tr, Tg, Tb) = get_pixel(&source, x+i-kh2, y+j-kw2);
					r += Tr*kernel(i, j);
					g += Tg*kernel(i, j);
					b += Tb*kernel(i, j);
				}
			result(x, y) = make_tuple(r, g, b);
		}        
	return result;  
}

Image gaussian(const Image &source, double sigma, double radius){
	int k = 2*radius + 1;
	Matrix<double> kernel(k, k);
	radius = k/2;
	double weight = 0;
	for (int x = 0; x < k; x++) 
		for (int y = 0; y < k; y++){
			kernel(x, y) = exp( -0.5 * (pow((x-radius), 2.0) + pow((y-radius), 2.0))/sigma/sigma )
						 / (2 * M_PI * sigma * sigma);
			weight += kernel(x, y);
		}
	for (int x = 0; x < k; x++) 
		for (int y = 0; y < k; y++)
			kernel(x, y) /= weight;

	return custom(source, kernel, false);
}

Image gaussian_separable(const Image &source, double sigma, double radius){
	int k = 2*radius + 1;
	Matrix<double> kernel0(1, k);
	Matrix<double> kernel1(k, 1);
	radius = k/2;
	double weight = 0;
	for (int y = 0; y < k; y++){
		kernel0(0, y) = exp( -0.5 * pow((y-radius)/sigma, 2.0) )
					 / (pow(2 * M_PI, 0.5) * sigma);
		weight += kernel0(0, y);
	}
	for (int y = 0; y < k; y++)
		kernel0(0, y) /= weight;
	weight = 0;
	for (int x = 0; x < k; x++){
		kernel1(x, 0) = exp( -0.5 * pow((x-radius)/sigma, 2.0) )
					 / (pow(2 * M_PI, 0.5) * sigma);
		weight += kernel1(x, 0);
	}
	for (int x = 0; x < k; x++)
		kernel1(x, 0) /= weight;
	// cout << kernel0 << kernel1;
	return custom(custom(source, kernel0, false), kernel1, false);
}

void replaceStrChar(std::string &str, char replace, char ch) {
  size_t i = str.find_first_of(replace);
  while (i != std::string::npos) { // while our position in the sting is in range.
	str[i] = ch; // change the character at position.
	i = str.find_first_of(replace, i+1); // relocate again.
  }
}

Image resize(const Image &source, double scale){ // bilinear
	uint nh = source.n_rows*scale, nw = source.n_cols*scale;
	int orows=source.n_rows, ocols=source.n_cols;
	int ncols=ocols*scale, nrows=orows*scale;
	double scrow = scale >= 1 ? scale : double(nrows-1)/(orows-1);
	double sccol = scale >= 1 ? scale : double(ncols-1)/(ocols-1);
	// cout << nrows << ' ' << ncols << endl;
 //    cout << scrow << ' ' << sccol;
	Image result(nh, nw);
	for(uint i = 0; i < nh; i++)
		for(uint j = 0; j < nw; j++){
			uint x1 = floor(1.0/scrow*i), x2 = x1 + 1, 
				y1 = floor(1.0/sccol*j), y2 = y1 + 1; 
					// indices of pixels from source
			double dx1 = 1.0/scrow*i - x1, dx2 = x2 - 1.0/scrow*i, 
				dy1 = 1.0/sccol*j - y1, dy2 = y2 - 1.0/sccol*j;
			result(i ,j) = normalize(get_pixel(&source, x1, y1)*dx2*dy2 + get_pixel(&source, x2, y1)*dx1*dy2 +
				get_pixel(&source, x1, y2)*dx2*dy1 + get_pixel(&source, x2, y2)*dx1*dy1);
		}
	return result;
}

Image autocontrast(const Image &source, double fraction){
	uint h = source.n_rows, w = source.n_cols;
	Image result(h, w);
	int histogram[256];
	for(uint i = 0; i < 256; i++)
		histogram[i] = 0;
	for(uint i = 0; i < h; i++)
		for (uint j = 0; j < w; j++)
			histogram[int(round(0.2125 * get<0>(source(i, j)) + 
								0.7154 * get<1>(source(i, j)) + 
								0.0721 * get<2>(source(i, j))))]++;

	uint bottom = 0, top = 255, sum = 0;
	for(uint i = 0; i < 256; i++){
		sum += histogram[i];
		if(sum > h*w*fraction){
			bottom = i;
			break;
		}
	}
	sum = 0;
	for(uint i = 256; i > 0; i--){
		sum += histogram[i-1];
		if(sum > h*w*fraction){
			top = i - 1;
			break;
		}
	}
	// uint edge = Y.size() * fraction;
	double Ymax = top, Ymin = bottom;
	// cout << Ymax << Ymin;
	if(Ymax < Ymin){
		throw std::string("autocontrast: Ymax < Ymin");
	}
	double coef = 255.0/(Ymax - Ymin);
	for(uint i = 0; i < h; i++)
		for(uint j = 0; j < w; j++){
			int R = std::get<0>(source(i,j));
			int G = std::get<1>(source(i,j));
			int B = std::get<2>(source(i,j));

			if(R < Ymin) 
				R = 0;
			else if(R > Ymax) 
				R = 255;
			else
				R = (R - Ymin)*coef;

			if(G < Ymin) 
				G = 0;
			else if(G > Ymax) 
				G = 255;
			else
				G = (G - Ymin)*coef;

			if(B < Ymin) 
				B = 0;
			else if(B > Ymax) 
				B = 255;
			else
				B = (B - Ymin)*coef;
			
			result(i, j) = normalize(make_tuple(R, G, B));
		}
	return result;
}